<?php

$conexao = mysqli_connect('localhost', 'root', '', 'dbRestaurante');


//SQL é a linguagem para se comunicar com o SGBD
function cadastrar($sql){
    global $conexao;
    
    $resultado = mysqli_query($conexao, $sql);
    
    if($resultado === true){
        echo "<script>
                  alert('Cadastro realizado com sucesso!');
                  history.back();
              </script>";
    }else{
        echo "<script>
                  alert('Erro ao realizar cadastro!');
                  history.back();
              </script>";        
    }   
}



function alterar($sql){
    global $conexao;
    
    $resultado = mysqli_query($conexao, $sql);
    
    if($resultado === true){
        echo "<script>
                  alert('Alteração realizada com sucesso!');
                  history.back();
              </script>";
    }else{
        echo "<script>
                  alert('Erro ao realizar alteração!');
                  history.back();
              </script>";        
    }   
}

function buscar($sql){
    global $conexao;
    
    //fetch_all transforma os dados do banco em um array
    $busca = mysqli_query($conexao, $sql)->fetch_all();
    
    //retorna os dados para o arquivo que chama a função
    return $busca;   
}


function deletar($sql){
    global $conexao;
    
    $resultado = mysqli_query($conexao, $sql);
    
    if($resultado === true){
        echo "<script>
                  alert('Registro excluido com sucesso!');
                  history.back();
              </script>";
    }else{
        echo "<script>
                  alert('Erro ao excluir registro!');
                  history.back();
              </script>";        
    }
}
