<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Restaurante Romaji</title>

        <link href="../css/admin.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>

    <body>
        
        <nav class="navbar navbar-expand-lg" style="background-color: red">
            <div class="col-md-11"></div>
            <a href="#" class="btn btn-warning col-md-1">Sair</a>
        </nav>
        
        <!--Início - Conteúdo da página administrador-->
        <div id="conteudoAdmin">
            <div id="menuAdmin" class="col-md-3">
                <br><br>
                <a href="inicioAdmin.php" class="col-md-12 text-white">Inicio</a><br>
                <a href="mesas.php" class="col-md-12 text-white">Mesas</a><br>
                <a href="#" class="col-md-12 text-white">Cliente</a><br>
                <a href="#" class="col-md-12 text-white">Reservas</a><br>
                
            </div>
                
            <div id="conteudo" class="col-md-9">

