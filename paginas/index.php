<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Restaurante Romaji</title>

        <link rel="stylesheet" href="../css/estilo.css"/>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>

    <body>
        <nav id="menu-inicio" class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <img src="../imagens/icone.png">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Preços</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">Reservas</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- Termina aqui nosso menu -->

        <!-- Começa aqui nossa página -->
        <div id="imagem-comida">
            <div id="pelicula">
                <h1>BEM-VINDO</h1>
                <a href="cadastre_se.php" type="button" class="btn btn-outline-light">CADASTRE-SE</a>
                <a href="login.php" type="button" class="btn btn-outline-light">LOGIN</a>
            </div>            
        </div>
        
        
        
        <div id="logo-cardapio">
            <img src="../imagens/logo111.png"/>
            <br>
            <a href="">CARDÁPIO</a>
        </div>
        
        <div id="interior">
            <div id="pelicula2">
                <h1>RESTAURANTE<br>ROMAJI</h1>
                <button class="btn btn-danger">FAÇA SUA RESERVA</button>
            </div>
        </div>
        
        
        
        <!--Início do rodapé-->
        <div id="rodape">
            <div>Políticas de Privacidade</div>
            
            
            <div>
                FONE:
                <br>
                (88) 9 9689-8675
            </div>
            
            
            <div>
                romaji@outlook.com
            </div>
            
        </div>
        <!--Fim do rodapé-->

        
        
        
    </body>
</html>

