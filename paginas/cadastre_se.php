<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Restaurante Romaji</title>

        <link rel="stylesheet" href="../css/estilo.css"/>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        <script src="../javascript/jquery.mask.js" type="text/javascript"></script>
    
        
        
        <script>
            $(document).ready(function(){
                $("#tel-com-mascara").mask("(00) 0000-0000");
            });
        </script>
    
        
        
    </head>

    <body>
        <div id="cadastre-se" class="container">
            <div class="alert alert-danger" role="alert">
                Atenção, após finalizar seu cadastro
                você precisa entrar em contato com
                nosso restaurante para liberar seu
                acesso!
            </div>
            
            
            
            

            <form action="../bd/cadastrarCliente.php">
                <h1>Faça seu Cadastro</h1>
                                
                <div class="form-group">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="nome">
                </div>
                
                
                <div class="form-group">
                    <label>Telefone</label>
                    <input id="tel-com-mascara" type="text" class="form-control" name="telefone">
                </div>
                
                
                <div class="form-group">
                    <label>Usuário</label>
                    <input type="text" class="form-control" name="usuario">
                </div>
                
                
                <div class="form-group">
                    <label>Senha</label>
                    <input type="password" class="form-control" name="senha">
                </div>
                
                <!--Início do botão com bootstrap-->
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-warning" href="index.php">Voltar</a>
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                    </div>                    
                </div>
                <!--Fim do botão com bootstrap-->
                
            </form>
            
            
            
            
        </div>


    </body>
</html>