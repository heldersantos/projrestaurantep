<?php
//inclui o código que está dentro do arquivo 
//cabecalho.php
include_once 'cabecalho.php';
?>


<br>
<div class="row">
    <div class="col-md-12">
        <!--botão-->
        <div class="col-md-6">
            <a href="" class="btn btn-primary">Novo Cliente</a>
        </div>

        <!--Campo de pesquisa!-->
        <br>
        <div class="col-md-6">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Pesquisar Cliente" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="button-addon2">Pesquisar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Tabela com a lista de clientes-->
<br>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr class="bg-primary text-white">
                    <td>Id</td>
                    <td>Nome</td>
                    <td>Telefone</td>
                    <td>Usuário</td>
                    <td>Opções</td>
                </tr>
            </thead>
            
            <tbody>
                                
                <?php
                $conexao = mysqli_connect('localhost', 'root', '', 'dbRestaurante');
                $clientes = mysqli_query($conexao, "SELECT * FROM cliente")->fetch_all();//fetch_all() converte os dados em array
                
                foreach ($clientes as $linha) {
                    echo "<tr>";
                        echo "<td>".  utf8_encode($linha[0])  ."</td>";
                        echo "<td>".  utf8_encode($linha[1])  ."</td>";
                        echo "<td>".  utf8_encode($linha[2])  ."</td>";
                        echo "<td>".  utf8_encode($linha[3])  ."</td>";
                        echo "<td>editar - excluir</td>";
                    echo "</tr>";
                }
                ?>
                
            </tbody>
        </table>
    </div>
</div>






<?php include_once 'rodape.php'; ?>
