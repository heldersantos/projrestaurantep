<?php include_once './cabecalho.php';?>

<center>
    <h1>Mesas</h1>
</center>


<form action="../bd/cadastrarMesa.php" method="POST">
    <div class="form-group">
        <label>Número da mesa</label>
        <input type="text" class="form-control" name="numMesa" required="">
    </div>
    
    <br>
    
    <button type="submit" class="btn btn-success">Cadastrar</button>
</form>

<hr>

<table class="table">
    <thead class="bg-primary text-white">
        <tr>
            <td>Número da Mesa</td>
            <td>Status</td>
            <td>Excluir</td>
        </tr>
    </thead>
    
    <tbody>
        
        <?php
        include_once '../bd/crud.php';
        
        $mesas = buscar("SELECT * FROM mesa");
        
        foreach ($mesas as $linha) {
            echo "<tr>";
              echo "<td>".$linha[1]."</td>";
              echo "<td>".$linha[2]."</td>";
              echo "<td>Excluir</td>";
            echo "</tr>";
        }
        
        ?>
        
        <tr>
            <td>001</td>
            <td>Reservada</td>
            <td>
                <a href="" class="btn btn-danger">Excluir</a>
            </td>
        </tr>
    </tbody>
</table>


<?php include_once './rodape.php';?>
